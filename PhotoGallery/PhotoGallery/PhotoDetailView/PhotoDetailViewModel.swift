//
//  PhotoDetailViewModel.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

final class PhotoDetailViewModel {
    
    // MARK: - Constants
    
    private let id: Int
    
    let title: String

    // MARK: - Properties
    
    private var photoDetailInfoService = PhotoDetailInfoService()
    
    // MARK: - Constructor
    
    init(with id: Int, title: String) {
        self.id = id
        self.title = title
    }

    // MARK: - Functions
    
    func updatePhotoDetailInfo(completion: @escaping (String) -> Void) {
        photoDetailInfoService.fetchPhotoDetainInfo(with: "\(id)") { detailInfo, error in
            if let error = error {
                // !!! toast
                print("PhotoDetailViewModel: \(#function): error = \(String(describing: error))")
            }
            completion(detailInfo?.imageURL ?? "")
        }
    }

    deinit {
        print("DEINIT: PhotoDetailViewModel")
    }
}
