//
//  PhotoDetailInfoService.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

typealias PhotoDetailInfoClosure = (PhotoListItem?, APIError?) -> Void

final class PhotoDetailInfoService {
    
    // MARK: - Properties
    
    private var apiSettings = APISettings()

    // MARK: - Functions
    
    func fetchPhotoDetainInfo(with id: String, completion: @escaping PhotoDetailInfoClosure) {
        let url = apiSettings.urlForRequest(apiRequestEndPoint: .photoDetailInfo, parameter: id)
        let session = URLSession(configuration: .default)
        
        session.dataTask(with: url) { data, response, error in
            if let error = error {
                let apiError = APIError(type: .httpError, descripton: error.localizedDescription)
                completion(nil, apiError)
                return
            }
            guard let response = response as? HTTPURLResponse,
                response.statusCode == 200 else {
                    let apiError = APIError(type: .httpError, descripton: APIErrorType.defaultDescripton)
                    completion(nil, apiError)
                    return
            }
            guard let data = data,
                let photoListItem = try? JSONDecoder().decode(PhotoListItem.self, from: data) else {
                    let apiError = APIError(type: .badData, descripton: APIErrorType.defaultDescripton)
                    completion(nil, apiError)
                return
            }
            completion(photoListItem, nil)
        }.resume()
    }

    deinit {
        print("DEINIT: PhotoDetailInfoService")
    }
}
