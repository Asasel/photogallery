//
//  PhotoDetailViewController.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit
import Nuke

final class PhotoDetailViewController: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet private weak var imageView: UIImageView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private(set) var viewModel: PhotoDetailViewModel!

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        viewModel.updatePhotoDetailInfo() { urlString in
            DispatchQueue.main.async { [weak self] in
                self?.activityIndicator.startAnimating()
                self?.loadImage(from: urlString)
            }
        }
    }
    
    deinit {
        print("DEINIT: PhotoDetailViewController")
    }

    // MARK: - Private functions
    
    private func configureUI() {
        titleLabel.text = viewModel.title
        hideBorder(false)
    }
    
    private func hideBorder(_ isHidden: Bool) {
        imageView.layer.borderColor = isHidden ? UIColor.clear.cgColor : #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        imageView.layer.borderWidth = isHidden ? 0.0 : 0.5
    }
    
    private func loadImage(from urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }        
        Nuke.loadImage(with: url, options: ImageLoadingOptions.shared, into: imageView) { [weak self] _ in
            self?.activityIndicator.stopAnimating()
            self?.hideBorder(true)
        }
    }
}

extension PhotoDetailViewController {
    
    static func create(with id: Int, title: String) -> PhotoDetailViewController {
        let viewController = PhotoDetailViewController.instantiateFromNib()
        viewController.viewModel = PhotoDetailViewModel(with: id, title: title)
        return viewController
    }
}
