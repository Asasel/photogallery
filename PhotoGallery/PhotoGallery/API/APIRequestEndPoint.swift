//
//  APIRequestEndPoint.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

enum APIRequestEndPoint: String {
    case
    photosList = "photos",
    photoDetailInfo = "photos/{%@}"
    
    func endPoint(with parameter: String) -> String {
        return rawValue.replacingOccurrences(of: "{%@}", with: "\(parameter)")
    }
}
