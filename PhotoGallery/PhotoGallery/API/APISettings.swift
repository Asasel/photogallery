//
//  APISettings.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

enum Environment: String {
    case
    jsonplaceholder = "jsonplaceholder.typicode.com"
}

final class APISettings {

    // MARK: - Properties

    private(set) var currentEnvironment: Environment

    private(set) var urlModel: URLModel

    // MARK: - Construction

    init() {
        self.currentEnvironment = .jsonplaceholder
        self.urlModel = URLModel(currentEnvironment: self.currentEnvironment)
    }

    // MARK: - Functions

    func urlForRequest(apiRequestEndPoint: APIRequestEndPoint, parameter: String = "") -> URL {
        return urlModel.configureUrl(for: apiRequestEndPoint, parameter: parameter)!
    }
}
