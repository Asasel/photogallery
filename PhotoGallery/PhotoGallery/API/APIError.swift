//
//  APIError.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

enum APIErrorType {
    case
    httpError,
    badData
    
    static var defaultDescripton: String {
        return """
            An error occurred during the request.
            Please, try later...
        """
    }
}

struct APIError {
    
    var type: APIErrorType

    var descripton: String?
}
