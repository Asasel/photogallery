//
//  URLModel.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

final class URLModel {
    
    // MARK: - Properties

    private var server: String

    private var currentEnvironment: Environment
    
    // MARK: - Construction

    init(currentEnvironment: Environment) {
        self.currentEnvironment = currentEnvironment
        self.server = currentEnvironment.rawValue
    }
    
    // MARK: - Functions

    func configureUrl(for apiRequestEndPoint: APIRequestEndPoint, parameter: String = "") -> URL? {
        let endPoint = parameter.isEmpty ? apiRequestEndPoint.rawValue : apiRequestEndPoint.endPoint(with: parameter)
        return URL(string: "https://\(server)/\(endPoint)")
    }
}
