//
//  PhotoListViewModel.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

typealias PhotoListViewItemDictionary = [Int : PhotoListItemViewModel]

protocol PhotoListViewModelMockable {
    func updatePhotoList(completion: @escaping ([PhotoListViewItemDictionary]) -> Void)
}

final class PhotoListViewModel: PhotoListViewModelMockable {
    
    // MARK: - Properties
    
    private var photoListService = PhotoListService()

    // MARK: - Functions
    
    func updatePhotoList(completion: @escaping ([PhotoListViewItemDictionary]) -> Void) {
        photoListService.fetchPhotoList { photoListItems, error in
            if let error = error {
                print("PhotoListViewModel: \(#function): error = \(String(describing: error))")
                completion([])
                return
            }
            let viewModels = photoListItems.compactMap {
                PhotoListViewItemDictionary(dictionaryLiteral: ($0.id,
                                                                PhotoListItemViewModel(title: $0.title,
                                                                                       thumbnailUrl: $0.thumbnailUrl)))
            }
            completion(viewModels)
        }
    }

    deinit {
        print("DEINIT: PhotoListViewModel")
    }
}
