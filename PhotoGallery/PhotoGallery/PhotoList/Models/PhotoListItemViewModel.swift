//
//  PhotoListItemViewModel.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

struct PhotoListItemViewModel {
    
    let title: String
    
    let thumbnailUrl: String
}
