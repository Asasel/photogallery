//
//  PhotoListService.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

typealias PhotoListClosure = ([PhotoListItem], APIError?) -> Void

final class PhotoListService {
    
    // MARK: - Properties
    
    private var apiSettings = APISettings()

    // MARK: - Functions
    
    func fetchPhotoList(completion: @escaping PhotoListClosure) {
        let url = apiSettings.urlForRequest(apiRequestEndPoint: .photosList)
        let session = URLSession(configuration: .default)
        session.dataTask(with: url) { data, response, error in
            if let error = error {
                let apiError = APIError(type: .httpError, descripton: error.localizedDescription)
                completion([], apiError)
                return
            }
            guard let response = response as? HTTPURLResponse,
                response.statusCode == 200 else {
                    let apiError = APIError(type: .httpError, descripton: APIErrorType.defaultDescripton)
                    completion([], apiError)
                    return
            }
            guard let data = data,
                let photoListItems = try? JSONDecoder().decode([PhotoListItem].self, from: data) else {
                    let apiError = APIError(type: .badData, descripton: APIErrorType.defaultDescripton)
                    completion([], apiError)
                return
            }
            completion(photoListItems, nil)
        }.resume()
    }

    deinit {
        print("DEINIT: PhotoListService")
    }
}
