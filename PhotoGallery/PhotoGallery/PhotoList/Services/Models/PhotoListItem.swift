//
//  PhotoListItem.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

struct PhotoListItem: Decodable {
    
    let id: Int

    let albumId: Int

    let title: String

    let imageURL: String

    let thumbnailUrl: String
    
    enum CodingKeys: String, CodingKey {
        case
        id,
        albumId,
        title,
        imageURL = "url",
        thumbnailUrl
    }
}
