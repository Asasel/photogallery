//
//  PhotoListCollectionViewCell.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit
import Nuke

final class PhotoListCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    @IBOutlet private weak var image: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Functions
    
    func loadImage(from urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        Nuke.loadImage(with: url, into: image)
    }
}
