//
//  PhotoListViewController.swift
//  PhotoGallery
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import UIKit

class PhotoListViewController: UIViewController {
    
    // MARK: - Constants
    
    let cellIdentifier = "PhotoListCollectionViewCell"
    
    let defaultHeight: CGFloat = 170.0
    
    let offset: CGFloat = 4.0
        
    let itemsInCompactRow = 3   // for iPhones, multiscreen mode
    
    let itemsInWideRow = 5 // for iPads

    // MARK: - Properties
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private var viewModel = PhotoListViewModel()
    
    private var cellViewModels = [PhotoListViewItemDictionary]()

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        viewModel.updatePhotoList { [weak self] viewModels in
            self?.cellViewModels = viewModels
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    
    deinit {
        print("DEINIT: PhotoListViewController")
    }

    // MARK: - Private functions
    
    private func configureUI() {
        navigationItem.title = "Photo Gallery"
        configureCollectionView()
    }
    
    private func configureCollectionView() {
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension PhotoListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                      for: indexPath) as! PhotoListCollectionViewCell
        let viewModel = cellViewModels[indexPath.row].values.first
        cell.titleLabel.text = viewModel?.title
        cell.loadImage(from: viewModel?.thumbnailUrl ?? "")
        return cell
    }
}

extension PhotoListViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = cellViewModels[indexPath.row]
        guard let id = model.keys.first,
            let title = cellViewModels[indexPath.row].values.first?.title  else {
            return
        }
        let detailViewController = PhotoDetailViewController.create(with: id, title: title)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

extension PhotoListViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsInRow = traitCollection.horizontalSizeClass == .compact ? itemsInCompactRow : itemsInWideRow
        return CGSize(width: collectionView.frame.width / CGFloat(itemsInRow) - offset,
                      height: defaultHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return offset
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
}

extension PhotoListViewController {
    
    static func create() -> PhotoListViewController {
        return PhotoListViewController.instantiateFromNib()
    }
}

