//
//  PhotoListViewModelMock.swift
//  PhotoGalleryTests
//
//  Created by Angelina Latash on 6/19/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import XCTest
@testable import PhotoGallery

class PhotoListViewModelMock: XCTestCase, PhotoListViewModelMockable {
    
    // MARK: - Functions
    
    func updatePhotoList(completion: @escaping ([PhotoListViewItemDictionary]) -> Void) {
        let photoListItem = PhotoListViewItemDictionary(dictionaryLiteral: (1,
                                                                            PhotoListItemViewModel(title: "test Title",
                                                                                                   thumbnailUrl: "testUrlString")))
        let photoListItem1 = PhotoListViewItemDictionary(dictionaryLiteral: (101,
                                                                            PhotoListItemViewModel(title: "test Title 1",
                                                                                                   thumbnailUrl: "testUrlString1")))
        let viewModels = [photoListItem, photoListItem1]
        completion(viewModels)
    }
}
