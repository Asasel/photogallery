//
//  PhotoGalleryTests.swift
//  PhotoGalleryTests
//
//  Created by Angelina Latash on 6/18/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class PhotoListViewModelTests: XCTestCase {
    
    var sut: PhotoListViewModelMock!
        
    var viewModels: [PhotoListViewItemDictionary]!

    override func setUp() {
        sut = PhotoListViewModelMock()
        
        let exp = expectation(description: "Retrieve photo list")
        sut.updatePhotoList { [unowned self] viewModels in
            self.viewModels = viewModels
            exp.fulfill()
        }
        waitForExpectations(timeout: 2)
    }

    override func tearDown() {
        sut = nil
        viewModels = nil
    }

    func testViewModelsCount() {
        XCTAssertEqual(viewModels.count, 2, "We have loaded exactly 2 photo list items")
    }
    
    func testPhotoListItem() {
        let testItem = viewModels[0]
        let identifier = Array(testItem.keys)[0]
        
        XCTAssertEqual(Array(testItem.keys).count, 1)
        XCTAssertEqual(identifier, 1)
        XCTAssertEqual(testItem[identifier]!.title, "test Title")
        XCTAssertEqual(testItem[identifier]!.thumbnailUrl, "testUrlString")
    }
}
