# README #

### Photo Gallery ###

1. retrieves the contents of the urls.
2. rarses and display the response in a list.
3. Tapping on an element in the list should navigate to a detail view.
4. The detail view displays the image.

### How do I get set up? ###

* Download the repository
* No need to install / update pods
* Run the project

### Contribution guidelines ###

* Compatible for iOS >=13.2
* Compatible for iPhones and iPads
* Uncludes unit tests for PhotoListViewModel

### What I have solved well in my opinion ###

* Choice of the architecture: MVVM
* No memory leaks
* No broken constraints
* behavior of PhotoListViewModel has been tested via unit tests

### What I would improve in a next step if I had more time ###

* Adding toasts for errors 
* Implement unit tests for PhotoDetailViewModel
